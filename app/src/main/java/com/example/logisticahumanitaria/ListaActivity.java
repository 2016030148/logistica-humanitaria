package com.example.logisticahumanitaria;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.Instrumentation;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import database.AgendaEstados;
import database.Estado;

public class ListaActivity extends ListActivity {


    private AgendaEstados agendaEstados;
    private Button btnNuevo;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        this.btnNuevo = (Button)findViewById(R.id.btnNuevo);
        this.agendaEstados = new AgendaEstados(this);
        this.agendaEstados.openDatabase();
        ArrayList<Estado> contactos = agendaEstados.allContactos();
        MyArrayAdapter adapter = new MyArrayAdapter(this,R.layout.activity_estado, contactos);
        setListAdapter(adapter);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ListaActivity.this,MainActivity.class);
                i.putExtra("id",0);
                startActivity(i);
                finish();
            }
        });
    }
    class MyArrayAdapter extends ArrayAdapter<Estado> {
        Context context;
        int textViewResourceId;
        ArrayList<Estado> objects;
        public MyArrayAdapter(Context context, int textViewResourceId,ArrayList<Estado> objects){
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewResourceId = textViewResourceId;
            this.objects = objects;
        }
        public View getView(final int position, View convertView, ViewGroup viewGroup){
            LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId, null);
            TextView lblNombre = (TextView)view.findViewById(R.id.lblNombreEstado);
            Button modificar = (Button)view.findViewById(R.id.btnModificar);
            Button borrar = (Button)view.findViewById(R.id.btnBorrar);
            lblNombre.setText(objects.get(position).getEstado());
            borrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    agendaEstados.openDatabase();
                    agendaEstados.deleteContacto(objects.get(position).get_ID());
                    agendaEstados.close();
                    objects.remove(position);
                    notifyDataSetChanged();
                }
            });
            modificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("estado",objects.get(position));
                    Intent i = new Intent();
                    i.putExtras(bundle);
                    setResult(Activity.RESULT_OK,i);
                    finish();
                }
            });
            return view;
        }
    }

}
