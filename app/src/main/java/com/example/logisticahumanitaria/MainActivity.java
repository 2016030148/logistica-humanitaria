package com.example.logisticahumanitaria;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import database.AgendaEstados;
import database.Estado;

public class MainActivity extends AppCompatActivity {
    private Button btnGuardar;
    private Button btnListar;
    private Button btnLimpiar;
    private EditText txtEstado;
    private Estado savedEstado;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /*WORKING*/
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnListar = (Button) findViewById(R.id.btnListar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        txtEstado = (EditText) findViewById(R.id.txtEstado);


        SharedPreferences sharedPreferences = getSharedPreferences("datos", Context.MODE_PRIVATE);
        boolean agregados = sharedPreferences.getBoolean("agregados",false);

        if(!agregados) {
            AgendaEstados agenda = new AgendaEstados(MainActivity.this);
            agenda.openDatabase();
            if (agenda.allContactos().size() == 0) {
                String[] estados = {"Sinaloa", "Sonora", "Chihuahua", "Veracruz", "Jalisco", "Nuevo León", "Coahuila", "Yucatán", "Oaxaca", "Chihuaha"};
                for (int x = 0; x < 10; x++) {
                    Estado e = new Estado();
                    e.setEstado(estados[x]);
                    agenda.insertContacto(e);
                }
            }
            agenda.close();
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean("agregados",true);
            editor.commit();
        }
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean completo = true;
                if (txtEstado.getText().toString().equals("")){
                    txtEstado.setError("Introduce el nombre");
                    completo = false;
                }
                if (completo){
                    AgendaEstados source = new AgendaEstados(MainActivity.this);
                    source.openDatabase();
                    Estado oEstado = new Estado();
                    oEstado.setEstado(txtEstado.getText().toString());
                    if (savedEstado == null){
                        source.insertContacto(oEstado);
                        Toast.makeText(MainActivity.this,"Contacto Guardado!",Toast.LENGTH_SHORT).show();
                        limpiar();
                    } else {
                        source.updateContacto(oEstado,id);
                        Toast.makeText(MainActivity.this,"Contacto Actualizado!", Toast.LENGTH_SHORT).show();
                        limpiar();
                    }
                    source.close();
                }

            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, ListaActivity.class);
                startActivityForResult(i, 0);
                limpiar();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiar();
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (Activity.RESULT_OK == resultCode){
            Estado estado = (Estado) data.getSerializableExtra("estado");
            savedEstado = estado;
            id = estado.get_ID();
            txtEstado.setText(estado.getEstado());
        }
    }

    public void limpiar(){
        txtEstado.setText("");
        txtEstado.setError(null);
    }
}
