package database;

import java.io.Serializable;

public class Estado implements Serializable {
    private int _ID;
    private String estado;

    public Estado(){
        this._ID = 0;
        this.estado = "";
    }
    public Estado(int _ID,String estado){
        this._ID = _ID;
        this.estado = estado;
    }

    public int get_ID() {
        return _ID;
    }

    public void set_ID(int _ID) {
        this._ID = _ID;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
